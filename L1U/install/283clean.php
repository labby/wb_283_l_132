<?php

/**
 *  @script		    upgrade WB 2.8.3 to LEPTON_1
 *  @version        see https://gitlab.com/labby/wb_281_l_132
 *  @author         cms-lab
 *  @copyright      2013-2018 CMS-LAB
 *  @license        http://creativecommons.org/licenses/by/3.0/
 *  @license terms  none
 *  @platform       WebsiteBaker 2.8.1
 */

define('DEBUG', true);

// set error level
 ini_set('display_errors', 1);
 error_reporting(E_ALL|E_STRICT);

// Include config file
$config_file = '../config.php';
if(file_exists($config_file))
{
	require_once($config_file);

} else {
	die("<h4 style='color:red;text-align:center;font-size:20px;'> cannot find any config.php </h4>");	// make sure that the code below will not be executed
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Upgrade from WB 2.8.3 to LEPTON 1.3.2</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<link href="http://lepton-cms.org/_packinstall/update.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="top">
  <div id="top-logo"></div>
  <div id="top-text">WB 283 to LEPTON132</div>
</div>
<div id="update-script">
<?php

/**
 *  database modification
 */

// install new modules
echo '<h5>Current process : install new modules</h5>';

if (!function_exists('load_module')) require_once( LEPTON_PATH."/framework/functions.php");

$install = array (
"/modules/output_interface",
"/modules/lib_jquery",
"/modules/wysiwyg_admin",
"/modules/initial_page",
"/modules/edit_area",
"/modules/pclzip",
"/modules/phpmailer",
"/modules/tiny_mce_jq"
);

// install new modules
foreach ($install as $module)
{
    $temp_path = LEPTON_PATH . $module ;

//require_once ($temp_path.'/info.php');
load_module( $temp_path, true );

foreach(
array(
'module_license', 'module_author'  , 'module_name', 'module_directory',
'module_version', 'module_function', 'module_description',
'module_platform', 'module_guid'
) as $varname )
{
if (isset(  ${$varname} ) ) unset( ${$varname} );
}
}
echo "<h3>install new modules: successfull</h3>";


// install new templates
echo '<h5>Current process : install new templates</h5>';

$install = array (
"/templates/algos",
"/templates/lepton"
);

	// install new templates
foreach ($install as $template)
{
    $temp_path = LEPTON_PATH . $template ;

require ($temp_path.'/info.php');
load_template( $temp_path, true );
	
foreach(
array(
'template_license', 'template_author'  , 'template_name', 'template_directory',
'template_version', 'template_function', 'template_description',
'template_platform', 'template_guid'
) as $varname )
{
if (isset(  ${$varname} ) ) unset( ${$varname} );
}
}		
echo "<h3>new templates install: successfull</h3>";


// run upgrade.php of all modified modules
echo '<h5>Current process : upgrade modified modules</h5>';
$upgrade_modules = array(
"form",
"edit_area",
"jsadmin",
"droplets",	
"addon_file_editor",
"captcha_control",
"code2",
"initial_page",
"lib_jquery",
"menu_link",
"news",
"output_interface",
"show_menu2",
"wrapper",
"wysiwyg",
"wysiwyg_admin"

);

foreach ($upgrade_modules as $module)
{
    $temp_path = LEPTON_PATH . "/modules/" . $module . "/upgrade.php";

    if (file_exists($temp_path))
        require($temp_path);
}
echo "<h3>upgrade of modified modules successfull</h3>";


/**
 *  reload all addons
 */
if (file_exists (LEPTON_PATH.'/install/update/reload.php')) {
			require_once(LEPTON_PATH . '/install/update/reload.php');
}


//  remove directory 
echo '<h5>Current process : delete L1U directory</h5>';

rm_full_dir(LEPTON_PATH."/L1U");
		
echo "<h3>delete L1U directory: successfull</h3>";


//  delete not needed files
echo '<h5>Current process : delete not needed files</h5>';

$to_move = array (
"/config_wb.php",
"/config_step1.php",
"/283_start.php",
"/283_step_2.php"
);

	// move used files
foreach ($to_move as $file)
{
    $temp_path = LEPTON_PATH .$file ;

	rename( $temp_path,LEPTON_PATH.'/install'.$file);
	
}		

echo "<h3>deleted not needed files successfully</h3>";

//  handle db fields from LEPTON 1.1.1 to 1.3.2
echo '<h5>Current process : database operations from 1.1.0 to 1.3.2</h5>';

// 132: GUID is part of config.php and not needed in database 
$database->query( "DELETE FROM `".TABLE_PREFIX."settings` WHERE `name`= 'lepton_guid'" );

// 114: try to remove obsolete column 'license_text'
$checkDbTable = $database->query("SHOW COLUMNS FROM `".TABLE_PREFIX."addons` LIKE 'license_text'");
$column_exists = $checkDbTable->numRows() > 0 ? TRUE : FALSE;

if (true === $column_exists ) {
 $database->query('ALTER TABLE `' . TABLE_PREFIX . 'addons` DROP COLUMN `license_text`');
}

//  1112: database modifications 
$all = $database->query(" SELECT * from `" . TABLE_PREFIX . "users` limit 1");
if ($all)
{
    $temp = $all->fetchRow(MYSQL_ASSOC);
    if (array_key_exists("remember_key", $temp))
    {
        $database->query('ALTER TABLE `' . TABLE_PREFIX . 'users` DROP COLUMN `remember_key`');
    }
}

$all = $database->query(" DELETE from `" . TABLE_PREFIX . "settings` WHERE name = 'smart_login'");

//  111: database modifications above 1.1.0
$all = $database->query(" SELECT * from `" . TABLE_PREFIX . "addons` limit 1");
if ($all)
{
    $temp = $all->fetchRow(MYSQL_ASSOC);
    if (array_key_exists("php_version", $temp))
    {
        $database->query('ALTER TABLE `' . TABLE_PREFIX . 'addons` DROP COLUMN `php_version`, DROP COLUMN `sql_version`');
    }
}

echo "<h3>database operations from 1.1.0 to 1.3.2</h3>";

// at last: set db to current release-no
$database->query('UPDATE `' . TABLE_PREFIX . 'settings` SET `value` =\'1.3.2\' WHERE `name` =\'lepton_version\'');

/**
 *  success message
 */
echo "<br /><h3>Congratulation, you have successfully upgraded to LEPTON 1.3.2</h3><br />";
echo "<br /><h3>Help us to maintain and develop this CMS</h3><br /><hr /><br />";
/**
 *  support info
 */
?>

<div style="text-align:center;">
<table style="text-align: left; width: 100%;" border="0" cellspacing="2" cellpadding="2">
<tbody>
<tr>
<td align="center" valign="middle"><h3>Please consider a donation to support LEPTON.<br /> <br /></h3></td>
</tr>
<tr>
<td style="text-align: center;" align="left" valign="middle"><form action="https://www.paypal.com/cgi-bin/webscr" method="post"><input name="cmd" type="hidden" value="_s-xclick" /> <input name="hosted_button_id" type="hidden" value="DF6TFNAE7F7DJ" /> <input alt="PayPal &mdash; The safer, easier way to donate online." name="submit" src="https://www.paypalobjects.com/en_US/GB/i/btn/btn_donateCC_LG.gif" type="image" /> <img src="https://www.paypalobjects.com/de_DE/i/scr/pixel.gif" border="0" alt="" width="1" height="1" /></form></td>
</tr>
</tbody>
</table>
</div>
<?php
echo "<br /><a href='http://www.lepton-cms.org/english/contact.php' target='_blank'><h3>or support LEPTON in another way </h3></a><br /><hr /><br />";

/**
 *  login message
 */

echo "<br /><h3><a href=' ".ADMIN_URL."/login/index.php'>please login and check installation</></h3>";
?>
</div>
<div id="update-footer">
      <!-- Please note: the below reference to the GNU GPL should not be removed, as it provides a link for users to read about warranty, etc. -->
      <a href="http://wwww.lepton-cms.org" title="LEPTON CMS">LEPTON Core</a> is released under the
      <a href="http://www.gnu.org/licenses/gpl.html" title="LEPTON Core is GPL">GNU General Public License</a>.
      <!-- Please note: the above reference to the GNU GPL should not be removed, as it provides a link for users to read about warranty, etc. -->
	    <br /><a href="http://wwww.lepton-cms.org" title="LEPTON CMS">LEPTON CMS Package</a> is released under several different licenses.
</div>
</body>
</html>