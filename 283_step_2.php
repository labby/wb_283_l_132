<?php

/**
 *  @script		    upgrade WB 2.8.3 to LEPTON_1
 *  @version        see https://gitlab.com/labby/wb_281_l_132
 *  @author         cms-lab
 *  @copyright      2013-2018 CMS-LAB
 *  @license        http://creativecommons.org/licenses/by/3.0/
 *  @license terms  none
 *  @platform       WebsiteBaker 2.8.1
 */


define('DEBUG', true);

// set error level
ini_set('display_errors', 1);
 error_reporting(E_ALL|E_STRICT);

// Include config file
$config_file = dirname(__FILE__).'/config.php';
if(file_exists($config_file))
{
	require_once($config_file);

} else {
	die("<h4 style='color:red;text-align:center;font-size:20px;'> cannot find any config.php </h4>");	// make sure that the code below will not be executed
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Upgrade from WB 2.8.3 to LEPTON 1.3.2</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<link href="http://lepton-cms.org/_packinstall/update.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="top">
  <div id="top-logo"></div>
  <div id="top-text">WB 283 to LEPTON132</div>
</div>
<div id="update-script">
<?php


echo '<h3>Upgrading to LEPTON 1.3.2 step 2 </h3>';
/**
 *  database modification 
 */
// set new table prefix
$new_prefix ="lep_"; 

// rename all tables from WB
echo '<h5>Current process : rename all wb tables</h5>'; 

 $ignore = TABLE_PREFIX;
 $tables = $database->list_tables( $ignore );

foreach($tables as $table) {
    $database->query("RENAME TABLE `".TABLE_PREFIX.$table."` TO `".$new_prefix.$table."`"); 
  if ($database->is_error())
  {
	  echo $datbase->get_error();
  }
  else
  {
	  echo '<h5>table: successfull</h5>';
  }
}
echo '<h5>rename all wb tables: successfull</h5>';

//add field guid in addons table
echo '<h5>Current process : add field guid in addons table</h5>';
	
	$all_fields = array();
	$database->describe_table( $new_prefix."addons", $all_fields );

	$look_up_fields = array(
	'guid'	=> "ADD `%s` VARCHAR( 50 ) NOT NULL DEFAULT '' "
	);

	foreach($all_fields as &$ref) {
	foreach($look_up_fields as $look_up_name=>$value) {
	
	if ($ref['Field'] == $look_up_name) $look_up_fields[ $look_up_name ] = false;
	}
	}

	foreach($look_up_fields as $key=>$val) {
	if (false === $val) continue;
	$database->query('ALTER TABLE `'.$new_prefix.'addons` '. sprintf($val, $key));
	}		
echo '<h5>add field guid in addons table: successfull</h5>';

//  modify table mod_news_posts
echo '<h5>Current process : modify table mod_news_posts</h5>'; 
$database->query( "ALTER TABLE `". $new_prefix ."mod_news_posts` DROP COLUMN `created_when`" );
$database->query( "ALTER TABLE `". $new_prefix ."mod_news_posts` DROP COLUMN `created_by`" );
echo '<h5>modify table mod_news_posts: successfull</h5>';

//add field name in sections table
echo '<h5>Current process : add field name in sections table</h5>';
	
	$all_fields = array();
	$database->describe_table( $new_prefix."sections", $all_fields );

	$look_up_fields = array(
	'name'	=> "ADD `%s` VARCHAR( 255 ) NOT NULL DEFAULT 'no name' "
	);

	foreach($all_fields as &$ref) {
	foreach($look_up_fields as $look_up_name=>$value) {
	
	if ($ref['Field'] == $look_up_name) $look_up_fields[ $look_up_name ] = false;
	}
	}

	foreach($look_up_fields as $key=>$val) {
	if (false === $val) continue;
	$database->query('ALTER TABLE `'.$new_prefix.'sections` '. sprintf($val, $key));
	}		
echo '<h5>add field name in sections table</h5>';

//  modfy table settings
echo '<h5>Current process : modify table settings</h5>'; 
$database->query( "DELETE FROM `". $new_prefix ."settings` WHERE `name`= 'wysiwyg_style'" );
$database->query( "DELETE FROM `". $new_prefix ."settings` WHERE `name`= 'default_timezone'" );
$database->query( "DELETE FROM `". $new_prefix ."settings` WHERE `name`= 'warn_page_leave'" );
$database->query( "DELETE FROM `". $new_prefix ."settings` WHERE `name`= 'smart_login'" );
$database->query( "DELETE FROM `". $new_prefix ."settings` WHERE `name`= 'fingerprint_with_ip_octets'" );
$database->query( "DELETE FROM `". $new_prefix ."settings` WHERE `name`= 'secure_form_module'" );
$database->query( "DELETE FROM `". $new_prefix ."settings` WHERE `name`= 'wb_version'" );
$database->query( "DELETE FROM `". $new_prefix ."settings` WHERE `name`= 'wb_revision'" );
$database->query( "DELETE FROM `". $new_prefix ."settings` WHERE `name`= 'wb_sp'" );
$database->query( "DELETE FROM `". $new_prefix ."settings` WHERE `name`= 'wb_secform_secret'" );
$database->query( "DELETE FROM `". $new_prefix ."settings` WHERE `name`= 'wb_secform_secrettime'" );
$database->query( "DELETE FROM `". $new_prefix ."settings` WHERE `name`= 'wb_secform_timeout'" );
$database->query( "DELETE FROM `". $new_prefix ."settings` WHERE `name`= 'wb_secform_tokenname'" );
$database->query( "DELETE FROM `". $new_prefix ."settings` WHERE `name`= 'wb_secform_usefp'" );
$database->query( "DELETE FROM `". $new_prefix ."settings` WHERE `name`= 'wb_secform_useip'" );


$database->query("UPDATE `" . $new_prefix ."settings` SET `value` ='lep_' WHERE `name` ='sec_anchor'");
$database->query("UPDATE `" . $new_prefix ."settings` SET `value` ='algos' WHERE `name` ='default_theme'");
$database->query("UPDATE `" . $new_prefix ."settings` SET `value` ='tiny_mce_jq' WHERE `name` ='wysiwyg_editor'");
$database->query("UPDATE `" . $new_prefix ."settings` SET `value` ='jpg,jpeg,gif,gz,png,pdf,tif,zip' WHERE `name` ='rename_files_on_upload'");
$database->query("UPDATE `" . $new_prefix ."settings` SET `value` ='8191' WHERE `name` ='er_level'");
$database->query("UPDATE `" . $new_prefix ."settings` SET `value` ='false' WHERE `name` ='page_languages'");


$app_name = $database->get_one("SELECT `value` from `". $new_prefix ."settings` WHERE `name`='app_name'");
if ($database->is_error()) {
 echo $datbase->get_error();
} else {
 $new_name = str_replace("wb_", "lep", $app_name);
 $database->query("UPDATE `". $new_prefix ."settings` SET `value`='". $new_name ."' WHERE `name`='app_name'");
}
echo '<h5>modify table settings: successfull</h5>';


//modify search settings
echo '<h5>Current process : modify search table</h5>';
$database->query("UPDATE `" . $new_prefix ."search` SET `value` ='false' WHERE `name` ='cfg_enable_old_search'");
echo '<h5>modify search table: successfull</h5>';


//modify users table
echo '<h5>Current process : modify users table</h5>';

$database->query( "ALTER TABLE `". $new_prefix ."users` DROP COLUMN `timezone`" );
	
	$all_fields = array();
	$database->describe_table( $new_prefix."users", $all_fields );

	$look_up_fields = array(
	'statusflags'	=> "ADD `%s` int( 11 ) NOT NULL DEFAULT '6'",
	'timezone_string'	=> "ADD `%s` VARCHAR( 50 ) NOT NULL DEFAULT 'Europe/Berlin'"
	);

	foreach($all_fields as &$ref) {
	foreach($look_up_fields as $look_up_name=>$value) {
	
	if ($ref['Field'] == $look_up_name) $look_up_fields[ $look_up_name ] = false;
	}
	}

	foreach($look_up_fields as $key=>$val) {
	if (false === $val) continue;
	$database->query('ALTER TABLE `'.$new_prefix.'users` '. sprintf($val, $key));
	}		
echo '<h5>modify users table</h5>';

echo '<br /><h3>All database modifications successfull</h3><br />';	

#############################  end of database modifications ############################################


//  write config file for LEPTON 
echo '<h5>Current process : ceate new config file</h5>';
// copy config file

$file = 'config.php';
$newfile = 'config_step1.php';

if (!copy($file, $newfile)) {
    echo "failed to copy $file...\n";
}


	// write settings to config file
$config_content = "" .
"<?php\n".
"\n".
"if(defined('LEPTON_PATH')) { die('By security reasons it is not permitted to load \'config.php\' twice!! ".
"Forbidden call from \''.\$_SERVER['SCRIPT_NAME'].'\'!'); }\n\n".
"\n\n// Upgraded from WebsiteBaker 2.8.3 to LEPTON 1.3.2\n".
"define('DB_TYPE', 'mysql');\n".
"define('DB_HOST', '".DB_HOST."');\n".
"define('DB_PORT', '".DB_PORT."');\n".
"define('DB_USERNAME', '".DB_USERNAME."');\n".
"define('DB_PASSWORD', '".DB_PASSWORD."');\n".
"define('DB_NAME', '".DB_NAME."');\n".
"define('TABLE_PREFIX', '".$new_prefix."');\n".
"\n".
"define('LEPTON_PATH', dirname(__FILE__));\n".
"define('LEPTON_URL', '".WB_URL."');\n".
"define('ADMIN_PATH', LEPTON_PATH.'/admins');\n".
"define('ADMIN_URL', LEPTON_URL.'/admins');\n".
"\n".
"define('LEPTON_GUID', '".LEPTON_GUID."');\n".
"define('LEPTON_SERVICE_FOR', '');\n".
"define('LEPTON_SERVICE_ACTIVE', 0);\n".
"define('WB_URL', LEPTON_URL);\n".
"define('WB_PATH', LEPTON_PATH);\n".
"\n".
"if (!defined('LEPTON_INSTALL')) require_once(LEPTON_PATH.'/framework/initialize.php');\n".
"\n".
"?>";



// Check if the file exists and is writable first.
$config_filename = 'config.php';
if(($handle = @fopen($config_filename, 'w')) === false) {
	set_error("Cannot open the configuration file ($config_filename)");
} else {
	if (fwrite($handle, $config_content, strlen($config_content) ) === false) {
		fclose($handle);
		set_error("Cannot write to the configuration file ($config_filename)");
	}
	// Close file
	fclose($handle);
}
echo "<h3>config written successfull</h3>";

/**
 *  success message
 */
echo "<br /><h3>Congratulation, upgrade procedure step 2 to LEPTON 1.3.2 complete!</h3><br /><hr /><br />";

/**
 *  support info
 */
?>

<div style="text-align:center;">
<table style="text-align: left; width: 100%;" border="0" cellspacing="2" cellpadding="2">
<tbody>
<tr>
<td align="center" valign="middle"><h3>Please consider a donation to support LEPTON.<br /> <br /></h3></td>
</tr>
<tr>
<td style="text-align: center;" align="left" valign="middle"><form action="https://www.paypal.com/cgi-bin/webscr" method="post"><input name="cmd" type="hidden" value="_s-xclick" /> <input name="hosted_button_id" type="hidden" value="DF6TFNAE7F7DJ" /> <input alt="PayPal &mdash; The safer, easier way to donate online." name="submit" src="https://www.paypalobjects.com/en_US/GB/i/btn/btn_donateCC_LG.gif" type="image" /> <img src="https://www.paypalobjects.com/de_DE/i/scr/pixel.gif" border="0" alt="" width="1" height="1" /></form></td>
</tr>
</tbody>
</table>
</div>
<?php
echo "<br /><a href='http://www.lepton-cms.org/english/contact.php' target='_blank'><h3>or support LEPTON in another way </h3></a><br /><hr /><br />";

/**
 *  do some cleanings
 */

 echo "<br /><h4 style='font-size:18px;'><a href='".LEPTON_URL."/install/283clean.php'>please click to remove not needed files</></h3>";
 
?>
</div>
<div id="update-footer">
      <!-- Please note: the below reference to the GNU GPL should not be removed, as it provides a link for users to read about warranty, etc. -->
      <a href="http://wwww.lepton-cms.org" title="LEPTON CMS">LEPTON Core</a> is released under the
      <a href="http://www.gnu.org/licenses/gpl.html" title="LEPTON Core is GPL">GNU General Public License</a>.
      <!-- Please note: the above reference to the GNU GPL should not be removed, as it provides a link for users to read about warranty, etc. -->
	    <br /><a href="http://wwww.lepton-cms.org" title="LEPTON CMS">LEPTON CMS Package</a> is released under several different licenses.
</div>
</body>
</html>