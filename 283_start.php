<?php

/**
 *  @script		    upgrade WB 2.8.3 to LEPTON_1
 *  @version        see https://gitlab.com/labby/wb_281_l_132
 *  @author         cms-lab
 *  @copyright      2013-2018 CMS-LAB
 *  @license        http://creativecommons.org/licenses/by/3.0/
 *  @license terms  none
 *  @platform       WebsiteBaker 2.8.1
 */


define('DEBUG', true);

// set error level
ini_set('display_errors', 1);
 error_reporting(E_ALL|E_STRICT);

// Include config file
$config_file = dirname(__FILE__).'/config.php';
if(file_exists($config_file))
{
	require_once($config_file);

} else {
	die("<h4 style='color:red;text-align:center;font-size:20px;'> cannot find any config.php </h4>");	// make sure that the code below will not be executed
}

if (!defined("LEPTON_PATH")) define("LEPTON_PATH", WB_PATH);
if (!defined("LEPTON_URL")) define("LEPTON_URL", WB_URL);


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Upgrade from WB 2.8.3 to LEPTON 1.3.2</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<link href="http://lepton-cms.org/_packinstall/update.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="top">
  <div id="top-logo"></div>
  <div id="top-text">WB283 to LEPTON132</div>
</div>
<div id="update-script">
<?php


//  LEPTON 2series , check release

$lepton_version = $database->get_one("SELECT `value` from `" . TABLE_PREFIX . "settings` where `name`='wb_version'");
if (version_compare($lepton_version, "2.8.3", "<>"))
{
    die("<h4>ERROR:NO UPGRADE POSSIBLE, your Version is : 	".$lepton_version." but you need 2.8.3 to run this script </h4>");
}
echo("<h3>Your WB Version is : ".$lepton_version.", upgrade to 1.3.2 possible </h3>");



// check if database has charset utf-8 

$sql_query = "SELECT `value` FROM `".TABLE_PREFIX."settings` WHERE `name`='default_charset'";
$current_charset = $database->get_one($sql_query);

if ($current_charset != 'utf-8')
{
    echo("<h4>Your charset is <strong>".$current_charset."</strong>, no upgrade possible </h4>");
    echo('<h4>LEPTON 2series need a <i>"utf8"</i> database</h4>');
    echo('<h4>please modify your database to <i>"utf8"</i> first</h4>');
    die('<h4>there are lots of tutorials on the net</h4>');
}

echo("<h3>Your database charset is ".$current_charset.", upgrade to 1.3.2 possible </h3>");


echo '<h3>Upgrading to LEPTON 1.3.1, first step</h3>';

if(file_exists('README.md')) {
	unlink('README.md');	
}

// set some database values to avoid notices in step 2
$database->query( "INSERT INTO `". TABLE_PREFIX ."settings` (`name`, `value`) VALUES 
('lepton_version', '1.3.1'),
('lepton_guid', '0'),
('backend_title', 'LEPTON CMS 1series'),
('prompt_mysql_errors', 'false'),
('default_timezone_string', 'Europe/Berlin'),
('leptoken_lifetime', '0'),
('max_attempts', '9'),
('enable_old_language_definitions', 'true');
" );

//  write config file for LEPTON
echo '<h5>Current process : create new config file</h5>';
// copy config file

$file = 'config.php';
$newfile = 'config_wb.php';

if (!copy($file, $newfile)) {
    echo "failed to copy ".$file."...\n";
}

// write settings to config file
$config_content = "" .
"<?php\n".
"\n".
"if(defined('LEPTON_PATH')) { die('By security reasons it is not permitted to load \'config.php\' twice!! ".
"Forbidden call from \''.\$_SERVER['SCRIPT_NAME'].'\'!'); }\n\n".
"\n\n// 1 step from wb 2.8.3\n".
"define('DB_TYPE', 'mysql');\n".
"define('DB_HOST', '".DB_HOST."');\n".
"define('DB_PORT', '3307');\n".
"define('DB_USERNAME', '".DB_USERNAME."');\n".
"define('DB_PASSWORD', '".DB_PASSWORD."');\n".
"define('DB_NAME', '".DB_NAME."');\n".
"define('TABLE_PREFIX', '".TABLE_PREFIX."');\n".
"\n".
"define('LEPTON_PATH', dirname(__FILE__));\n".
"define('LEPTON_URL', '".WB_URL."');\n".
"define('ADMIN_PATH', LEPTON_PATH.'/admins');\n".
"define('ADMIN_URL', LEPTON_URL.'/admins');\n".
"\n".
"define('LEPTON_GUID', '1F9AA1F4-0283-0283-0283-D8B09259F45E');\n".
"define('LEPTON_SERVICE_FOR', '');\n".
"define('LEPTON_SERVICE_ACTIVE', 0);\n".
"define('WB_URL', LEPTON_URL);\n".
"define('WB_PATH', LEPTON_PATH);\n".
"\n".
"if (!defined('LEPTON_INSTALL')) require_once(LEPTON_PATH.'/framework/initialize.php');\n".
"\n".
"?>";



// Check if the file exists and is writable first.
$config_filename = 'config.php';
if(($handle = @fopen($config_filename, 'w')) === false) {
	set_error("Cannot open the configuration file (".$config_filename.")");
} else {
	if (fwrite($handle, $config_content, strlen($config_content) ) === false) {
		fclose($handle);
		set_error("Cannot write to the configuration file (".$config_filename.")");
	}
	// Close file
	fclose($handle);
}
echo "<h3>write new config file: successfull</h3>";


//  remove WB directories 
echo '<h5>Current process : delete WB directories</h5>';

if (!function_exists("rm_full_dir")) require_once(WB_PATH."/framework/functions.php");

$to_delete = array (
"/account",
"/admin",
"/framework",
"/include",
"/languages",
"/search",
"/temp"
);

foreach ($to_delete as $del)
{
    $temp_path = WB_PATH . $del . "/index.php";


    if (file_exists($temp_path)) 
		{
    	rm_full_dir( WB_PATH . $del );
		} else {
				echo ("<h4 style='color:orange;text-align:center;font-size:16px;'> directory $del not exists</h4>");
				}
}		
echo "<h3>delete directories: successfull</h3>";

//  remove WB module directories 
echo '<h5>Current process : delete WB module/template directories</h5>';

//check, if code2 is installed
// we need code2 table to run upgrade.php later on instead of install.php to save table entries
$code_file = WB_PATH.'modules/code2/info.php';
if(!file_exists($code_file))
{
	$table = TABLE_PREFIX."mod_code2";

/**
 *	Creating the table new
 */
$query  = "CREATE TABLE IF NOT EXISTS `".$table."` (";
$query .= "`section_id`	INT NOT NULL DEFAULT '0',";
$query .= "`page_id`	INT NOT NULL DEFAULT '0',";
$query .= "`whatis`		INT NOT NULL DEFAULT '0',";
$query .= "`content`	TEXT NOT NULL,";
$query .= " PRIMARY KEY ( `section_id` ) )";

$database->query( $query );

if ( $database->is_error() ) 
	$admin->print_error($database->get_error(), $js_back);
}

$to_delete = array (
"/modules/addon_file_editor",
"/modules/captcha_control",
"/modules/code2",
"/modules/droplets",
"/modules/form",
"/modules/jsadmin",
"/modules/lib_jquery",
"/modules/menu_link",
"/modules/news",
"/modules/SecureFormSwitcher",
"/modules/show_menu2",
"/modules/tiny_mce_jq",
"/modules/wrapper",
"/modules/wysiwyg",
"/templates/argos_theme",
"/templates/wb_theme"
);
 
foreach ($to_delete as $del)
{
    $temp_path = WB_PATH . $del . "/index.php";


    if (file_exists($temp_path)) 
		{
    	rm_full_dir( WB_PATH . $del );
		} else {
				echo ("<h4 style='color:orange;text-align:center;font-size:16px;'> Notice: directory $del not exists</h4>");
				}
}		
echo "<h3>delete module/template directories: successfull</h3>";


//  remove WB single files 
echo '<h5>Current process : delete WB single files</h5>';

$to_delete = array (
"/index.php",
"/media/index.php",
"/modules/admin.php",
"/modules/edit_module_files.php",
"/modules/index.php",
"/pages/index.php",
"/templates/index.php"
);
 
foreach ($to_delete as $del)
{
    $temp_path = WB_PATH . $del;


    if (file_exists($temp_path)) 
		{
    	rm_full_dir( WB_PATH . $del );
		} else {
				echo ("<h4 style='color:orange;text-align:center;font-size:16px;'> directory $del not exists</h4>");
				}
}		
echo "<h3>delete WB single files: successfull</h3>";

//  move new directories
echo '<h5>Current process : move LEPTON directories</h5>';

$to_move = array (
'account',
'admins',
'framework',
'include',
'install',
'languages',
'search',
'temp'

);

foreach ($to_move as $file)
{
    $temp_path = WB_PATH .'/L1U/'. $file ;

	rename( $temp_path,dirname(__FILE__).'/'.$file);
	
}		

echo "<h3>move LEPTON directories: successfull</h3>";

//  move new directories
echo '<h5>Current process : move LEPTON templates and modules</h5>';

$to_move = array (
"/templates/algos",
"/templates/lepton",
"/modules/addon_file_editor",
"/modules/captcha_control",
"/modules/code2",
"/modules/droplets",
"/modules/edit_area",
"/modules/form",
"/modules/initial_page",
"/modules/jsadmin",
"/modules/lib_jquery",
"/modules/menu_link",
"/modules/news",
"/modules/output_interface",
"/modules/pclzip",
"/modules/phpmailer",
"/modules/show_menu2",
"/modules/tiny_mce_jq",
"/modules/wrapper",
"/modules/wysiwyg",
"/modules/wysiwyg_admin"

);

foreach ($to_move as $file)
{
    $temp_path = WB_PATH .'/L1U'. $file ;

	rename( $temp_path,dirname(__FILE__).$file);
	
}		

echo "<h3>move LEPTON templates and modules: successfull</h3>";

//  move new single files
echo '<h5>Current process : move LEPTON single files</h5>';

$to_move = array (
"/index.php",
"/media/index.php",
"/modules/admin.php",
"/modules/edit_module_files.php",
"/modules/index.php",
"/pages/index.php",
"/templates/index.php"
);

foreach ($to_move as $file)
{
    $temp_path = WB_PATH .'/L1U'. $file ;

	rename( $temp_path,dirname(__FILE__).$file);
	
}		

echo "<h3>move LEPTON single files: successfull</h3>";


/**
 *  success message
 */
echo "<br /><h3>Congratulation, upgrade procedure step 1 to LEPTON 1.3.2 complete!</h3><br /><hr /><br />";

echo "<br /><h4 style='color:blue;'>Please check now DB_PORT in your config.php (line 10)!<br />Use '3307' for mobile server or '3306' for other servers </h3><br /><hr /><br />";

/**
 *  support info
 */
?>

<div style="text-align:center;">
<table style="text-align: left; width: 100%;" border="0" cellspacing="2" cellpadding="2">
<tbody>
<tr>
<td align="center" valign="middle"><h3>Please consider a donation to support LEPTON.<br /> <br /></h3></td>
</tr>
<tr>
<td style="text-align: center;" align="left" valign="middle"><form action="https://www.paypal.com/cgi-bin/webscr" method="post"><input name="cmd" type="hidden" value="_s-xclick" /> <input name="hosted_button_id" type="hidden" value="DF6TFNAE7F7DJ" /> <input alt="PayPal &mdash; The safer, easier way to donate online." name="submit" src="https://www.paypalobjects.com/en_US/GB/i/btn/btn_donateCC_LG.gif" type="image" /> <img src="https://www.paypalobjects.com/de_DE/i/scr/pixel.gif" border="0" alt="" width="1" height="1" /></form></td>
</tr>
</tbody>
</table>
</div>
<?php
echo "<br /><a href='http://www.lepton-cms.org/english/contact.php' target='_blank'><h3>or support LEPTON in another way </h3></a><br /><hr /><br />";

/**
 *  now going to step 2
 */

 echo "<br /><h4 style='font-size:18px;'><a href='283_step_2.php'>please click to start step 2</></h3>";
 
?>
</div>
<div id="update-footer">
      <!-- Please note: the below reference to the GNU GPL should not be removed, as it provides a link for users to read about warranty, etc. -->
      <a href="http://wwww.lepton-cms.org" title="LEPTON CMS">LEPTON Core</a> is released under the
      <a href="http://www.gnu.org/licenses/gpl.html" title="LEPTON Core is GPL">GNU General Public License</a>.
      <!-- Please note: the above reference to the GNU GPL should not be removed, as it provides a link for users to read about warranty, etc. -->
	    <br /><a href="http://wwww.lepton-cms.org" title="LEPTON CMS">LEPTON CMS Package</a> is released under several different licenses.
</div>
</body>
</html>